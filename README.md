Este proyecto es una estructura API tipo REST de pruebas con las siguientes funcionalidades:

JWT
Eslint
Unit Testing - Jest
Versioning
Logger - Winston
Mongoose
Documentation - Swagger

Esta desarrollada en Node Js, Express con una conexión a base de datos en MongoDB

Además funcionará para probar funcionalidades de Gitlab como Issues y CI