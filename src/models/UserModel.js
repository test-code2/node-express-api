const mongoose = require('mongoose')
const { Schema } = mongoose

// Define User Schema
const UsersSchema = new Schema({
  _id: mongoose.Schema.ObjectId,
  name: String,
  email: String,
  password: String,
  role: String,
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    default: Date.now
  },
  status: { type: Number, default: 1 }
}, { versionKey: false })

module.exports = mongoose.model('users', UsersSchema)
