// Set up mongoose connection
const mongoose = require('mongoose')
mongoose.set('useFindAndModify', false)
let urlConnect = ''

// important create .env file with data connection

if (process.env.APP_ENV === 'development') {
  // development connection
  urlConnect = `mongodb://localhost/${process.env.DB_NAME}`
} else {
  // production connection
  urlConnect = 'mongodb://DB_USER:DB_PASS@DB_HOST/DB_NAME?authSource=DB_NAME&w=1'
  urlConnect = urlConnect.replace('DB_USER', process.env.DB_NAME)
  urlConnect = urlConnect.replace('DB_PASS', process.env.DB_PASS)
  urlConnect = urlConnect.replace('DB_HOST', process.env.DB_HOST)
}

// connect with mongo database
mongoose
  .connect(urlConnect, {
    useUnifiedTopology: true,
    useNewUrlParser: true
  })
  .catch(err => {
    console.error(`MongoDB connection error: ${err.message}`)
  })
