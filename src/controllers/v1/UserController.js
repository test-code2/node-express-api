const mongoose = require('mongoose')
const UserModel = require('../../models/UserModel')

// get user list
// some filters can be applied
exports.list = (req, res, next) => {
  try {
    const roleFilter = req.query.role
    const skip = Number(req.query.skip) || 0
    const limit = Number(req.query.limit) || 0
    const fields = 'name email role status'

    let dataFilter = {}
    if (roleFilter) {
      // validar que el role sea válido
      if (!['admin', 'maintainer', 'reporter'].includes(roleFilter)) {
        return res.status(400).json({
          message: 'Invalid role',
          data: {}
        })
      }
      dataFilter = {
        role: roleFilter
      }
    }
    console.log('dataFilter :>> ', dataFilter)
    return UserModel.find(dataFilter, fields)
      .skip(skip)
      .limit(limit)
      .then(result => {
        if (!result || result.length === 0) {
          return res.status(404).json({
            message: 'Users not found',
            data: {}
          })
        }
        return UserModel.countDocuments(dataFilter, (err, count) => {
          const pagination = {}
          if (skip - limit >= 0) {
            pagination.previous = `/user?limit=${limit}&skip=${(skip - limit)}`
          }
          if (skip + limit < count) {
            pagination.next = `/user?limit=${limit}&skip=${skip + limit}`
          }
          
          res.status(200).json({
            message: 'Users found',
            data: {
              total: count,
              users: result
            },
            pagination
          })
        })
      })
      .catch(err => next(err))
  } catch (err) {
    next(err)
  }
}

// get user detail
exports.detail = (req, res, next) => {
  try {
    // some filters
    const userId = req.params.userId
    return UserModel.find({
      _id: mongoose.Types.ObjectId(userId)
    })
      .then(result => {
        if (!result || result.length === 0) {
          return res.status(404).json({
            message: 'User not found',
            data: {}
          })
        }
        return res.status(200).json({
          message: 'User found',
          data: {
            user: result
          }
        })
      })
      .catch(err => next(err))
  } catch (err) {
    next(err)
  }
}

// create new user
exports.create = (req, res, next) => {
  try {
    const { name, email, password, role } = req.body

    return new UserModel({
      _id: mongoose.Types.ObjectId(),
      name,
      email,
      password,
      role
    })
      .save()
      .then(result => {
        return res.status(200).json({
          message: 'User created',
          data: {
            user: result
          }
        })
      })
      .catch(err => next(err))
  } catch (err) {
    next(err)
  }
}

// update data user
// nota validar que exista el usuario
exports.update = (req, res, next) => {
  try {
    const { name, email, password, role } = req.body

    const userId = req.params.userId
    const dataUpdate = {}

    if (name) {
      dataUpdate.name = name
    }
    if (email) {
      dataUpdate.email = email
    }
    if (password) {
      dataUpdate.password = password
    }
    if (role) {
      dataUpdate.role = role
    }

    return UserModel.findOneAndUpdate(
      // filter
      { _id: userId },
      // data update
      { $set: dataUpdate }
    )
      .then(result => {
        if (!result || result.length === 0) {
          return res.status(404).json({
            message: 'User not found',
            data: {}
          })
        }
        return res.status(200).json({
          message: 'User updated',
          data: {
            user: result
          }
        })
      })
      .catch(err => next(err))
  } catch (err) {
    next(err)
  }
}

// delete data user
// nota validar que exista el usuario
exports.delete = (req, res, next) => {
  try {
    const userId = req.params.userId

    return UserModel.findOneAndDelete(
      { _id: userId }
    )
      .then(result => {
        if (!result || result.length === 0) {
          return res.status(404).json({
            message: 'User not found',
            data: {}
          })
        }
        return res.status(200).json({
          message: 'User deleted',
          data: {
            user: result
          }
        })
      })
      .catch(err => next(err))
  } catch (err) {
    next(err)
  }
}
