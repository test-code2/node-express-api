const express = require('express')
const router = express.Router()
const UserController = require('./../../controllers/v2/UserController')

// get user list
router.get('/', UserController.list)

// get one user
router.get('/:userId', UserController.detail)

// create
router.post('/', UserController.create)

// update
router.put('/:userId', UserController.update)

// delete
router.put('/:userId', UserController.delete)

// delete
router.delete('/:userId', UserController.delete)

module.exports = router
