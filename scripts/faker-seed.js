// http://marak.github.io/faker.js

// sólo nombres en español
const faker = require('faker/locale/es')
const mongo = require('mongodb').MongoClient
const assert = require('assert')
// const _ = require('lodash'); // puede servir en db con más de una collection

const url = 'mongodb://localhost:27017'
const dbName = 'test_db_rest_api'
const numberOfUsersToCreate = process.argv[2] || 10

mongo.connect(url, function (err, client) {
  assert.equal(null, err)
  const db = client.db(dbName)
  // get access to the relevant collections
  const usersCollection = db.collection('users')
  // make a bunch of users
  const users = []
  for (let i = 0; i < numberOfUsersToCreate; i++) {
    // note: gender selection for firstName not working
    const firstName = faker.name.firstName()
    const lastName = faker.name.lastName()
    const email = faker.internet.email(firstName, lastName).toLowerCase()
    const role = faker.random.arrayElement(['reporter', 'maintainer', 'admin'])
    const newUser = {
      email,
      role,
      status: 1,
      password: 'pass123',
      name: firstName + ' ' + lastName
    }
    users.push(newUser)
    console.log(newUser.email, '-', newUser.role)
  }
  usersCollection.insertMany(users)

  // dejo este código como ejemplo de cómo se puede usar lodash para relacionar dos collections

  //   let posts = [];
  //   for (let i = 0; i < 5000; i += 1) {
  //     let newPost = {
  //       title: faker.lorem.words(7),
  //       body: faker.lorem.words(500),
  //       // use lodash to pick a random user as the author of this post
  //       author: _.sample(users),
  //       // use lodash to add a random subset of the users to this post
  //       likes: _.sampleSize(users, Math.round(Math.random * users.length)).map(
  //         user => user._id
  //       )
  //     };
  //     posts.push(newPost);
  //     console.log(newPost.title);
  //   }
  //   postsCollection.insertMany(posts);
  console.log('\nDatabase seeded!')
  client.close()
})
