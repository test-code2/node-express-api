const express = require('express')
const bodyParser = require('body-parser')
require('dotenv').config()
// MongoDB connection
require('./src/helpers/db-connection')
const app = express()

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// init view
app.get('/', function (req, res) {
  res.send('Inicio')
})

// versioning routes
const v1Routes = require('./src/routes/v1')
const v2Routes = require('./src/routes/v2')
app.use('/v1', v1Routes)
app.use('/v2', v2Routes)

// specify default version
app.use('/', v1Routes)

// handler error
app.use(function (err, req, res, next) {
  if (!err.statusCode) err.statusCode = 500 // Sets a generic server error status code if none is part of the err
  return res.status(err.statusCode).json({
    message: err.message,
    data: {}
  })
})

// not found
app.get('*', function (req, res) {
  res.status(404).send('Not Found')
})

// server listening
app.listen(3000, () => {
  console.log('Server listening on port 3000')
})
